package com.ilabservice.darwin.darwincas.controller;

import com.alibaba.fastjson.JSONObject;
import com.ilabservice.darwin.darwincas.entity.User;
import com.ilabservice.darwin.darwincas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@RestController
public class UserController {

    @Resource(name = "userService")
    private final UserService service;


    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @Value("${cas-logout-url}")
    private String casLogoutUrl;



    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpSession httpSession, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelMap modelMap) throws IOException {

        httpSession.removeAttribute("musersession");
        httpSession.removeAttribute("_const_cas_assertion_");
        httpSession.invalidate();
        httpServletResponse.sendRedirect(casLogoutUrl);
    }

//    @RequestMapping(value = "/darwin", method = RequestMethod.GET)
//    public void darwin(HttpServletRequest request, HttpServletResponse response){
//        return "index.html";
//    }

    @RequestMapping(value = "/getPushers", method = RequestMethod.GET)
    public String getPushers(@RequestParam("pass") String password) {

        JSONObject defjson = new JSONObject();
        defjson.put("total",0);
        defjson.put("rows",new String[]{});

        List<User> user = service.selectAll();
        if(user ==null || StringUtils.isEmpty(password)) return defjson.toJSONString();
        if(user.stream().anyMatch(u->u.getPassword().equalsIgnoreCase(password)))
            return restTemplate.getForObject("http://localhost:10008/api/v1/pushers", String.class);
        else
            return defjson.toJSONString();
    }

    @ResponseBody
    @RequestMapping(path = "/verify",method = RequestMethod.POST)
    public ResponseEntity verifyPlainPwd(@RequestParam("pass")String password){
        List<User> user = service.selectAll();
        if(user ==null || StringUtils.isEmpty(password)) ResponseEntity.status(HttpStatus.BAD_REQUEST).body("verify failed");
        if(user.stream().anyMatch(u->u.getPassword().equalsIgnoreCase(password)))
            return ResponseEntity.status(HttpStatus.OK).body("ok");
        else
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("verify failed");
    }

//    @RequestMapping(value = "/api/v1/pushers", method = RequestMethod.GET)
//    public String apiPushers() {
//        return restTemplate.getForObject("http://localhost:10008/api/v1/pushers", String.class);
//    }

//    public User theFirst() {
//        return service.selectAll().get(0);
//    }

//    @RequestMapping(value = "/createUser", method = RequestMethod.POST)
//    @Transactional
//    public String createUser(@RequestParam("name")String name, @RequestParam("password")String password) throws Exception {
//
//        User user = new User();
//        //String password = "ilabservice123456";
//        user.setId(UUID.randomUUID().toString());
//        user.setUsername(name);
//        user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
//        if(service.insert(user)){
//            return "user " + user +  "created";
//        }else
//            return "Could not create the user";
//
//    }
//
//    @RequestMapping(value = "/deleteuser", method = RequestMethod.POST)
//    @Transactional
//    public String deleteUser() throws Exception {
//        if(service.deleteByName("ilabservice")){ return "successfully delete user ";}
//                else return "fail to delete user" ;
//    }
//
//    @RequestMapping(value = "user/{name}",method = RequestMethod.GET)
//    public User getUserByName(@PathVariable("name") String name){
//        return service.select(name);
//    }
//
//    @RequestMapping(value = "/updateUserPassword", method = RequestMethod.POST)
//    @Transactional
//    public String updateUserPassword(@RequestParam("name")String name, @RequestParam("password")String password){
//
//            if(StringUtils.isEmpty(name)|| StringUtils.isEmpty(password)){
//                return "user name / password can not be empty ...";
//
//            }
//
//            if(service.updatePassword(DigestUtils.md5DigestAsHex(password.getBytes()),name))
//                return " User password updated " + name;
//            else
//                return "fail to update the user passwor " + name;
//    }

}
